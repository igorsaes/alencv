<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function create()
    {
        return view('login.login');
    }
    public function store(LoginRequest $request)
    {
        /*$attributes = request()->validate([
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ]);

        if (auth()->attempt($attributes)) {
            return redirect('/');
        }

        throw ValidationException::withMessages([
            'email' => 'Incorrect email or password'
        ]);*/

        $attributes = $request->validated();
        if (auth()->attempt($attributes)) {
            return redirect('/');
        }
        throw ValidationException::withMessages([
            'email' => 'Incorrect email or password'
        ]);
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }
}
