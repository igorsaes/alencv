<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    public function create() {
        return view('register.register');
    }

    public function store(RegisterRequest $request){
        /*$attributes = request()->validate([
            'name'=>'required|min:5',
            'email'=>'required|email|unique:users,email',
            'password'=>'required'

        ]);*/
        
        $attributes = $request->validated();
       
        $attributes['password'] = bcrypt($attributes['password']);
        

        $user = User::create($attributes);
       
        auth()->login($user);

        return redirect('/');

    } 
}
    
