<!doctype html>
<html lang="en">

<head>
    <title>Register page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>

    <div class="container">

        <div class="border border-primary m-5 p-5">
            <h2 class="font-weight-bold text-center text-xl pb-5">Register</h2>
            <form method="POST" action="/register">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
                    @error('name')
                    <p class="text-danger text-xs mt-1">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">

                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{old('email')}}"
                        aria-describedby="emailHelp">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                        else.</small>
                    @error('email')
                    <p class="text-danger text-xs mt-1">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password">

                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </div>








</body>

</html>