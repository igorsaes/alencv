<!doctype html>
<html lang="en">

<head>
    <title>Home page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        @auth
        <p>Welcome {{auth()->user()->name}}</p>
        <form method="POST" action="/logout">
            @csrf
            <h1 class="mt-5 pt-5"><button type="submit" class="btn btn-primary">Logout</button></h1>
        </form>
        @else

        <h1><a href="/register">Register</a></h1>

        <h1 class="mt-5 pt-5"><a href="/login">Login</a></h1>
        @endauth
    </div>








</body>

</html>